import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import './App.css';
 
class SingleJoke extends Component {


  constructor(props) {
    super(props);
    this.state = { joke: [] };
    this.goBack = this.goBack.bind(this); // i think you are missing this

}
goBack(){
  this.props.history.goBack();
}

showData() {
  if (this.props.location.state == undefined){return <Redirect to='/feed' />  }
    return (
        <div >
            <div style={{marginBottom:25}}> <Button onClick={this.goBack} variant="contained" color="primary"> Go Back</Button> </div>
            {this.props.location.state.value}
        </div>
   )
  
}

  render() {
    return (

    <Paper   elevation={5} style={{margin:40,padding:25}}>
        <Typography variant="h3" component="h3">{this.showData()}  </Typography>  
        <Typography component="p">jokes from chucknorris.io.</Typography>     
    </Paper>

    );
  }
}

 export default SingleJoke;