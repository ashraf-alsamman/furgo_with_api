import React, { Component } from 'react';
import axios from 'axios';
 import { Link } from 'react-router-dom'
import './App.css';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/Inbox';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import LoadingScreen from 'react-loading-screen';

class Feed extends Component {


  constructor(props) {
    super(props);
    this.state = { jokes: [] , category:null,categories:[],filter:[] ,loading:false};

   // Binds our scroll event handler
   window.onscroll = () => {
 
    // Checks that the page has scrolled to the bottom

    if ((window.innerHeight + window.scrollY+5) >= document.body.offsetHeight ) {
        this.getJokes(3);
    }
     
  };


}


 
componentWillMount  () {
   this.getJokes(5);
   this.getCategories();
 }

 Filter  (category) {
   this.setState({  jokes: [],category: category ,ref:false  } ,()=> {
    this.getJokes(3);
    })
 }

getJokes(num){
  this.setState({ loading:true});

  let chucknorris ;
  if (this.state.category == null){  chucknorris = 'https://furgoapi.000webhostapp.com/public/api/GetRandom/'+num}
  else{  chucknorris = 'https://furgoapi.000webhostapp.com/public/api/GetByCategory/'+this.state.category+'/'+num}
  
 
         axios.get(chucknorris).then(responce=>{
          let joined = this.state.jokes.concat(responce.data);
           this.setState({ jokes: joined ,loading:false});

     }).catch(error=>{  console.log(error)   })
   
 }

  getCategories() {
   axios.get('https://api.chucknorris.io/jokes/categories').then(responce=>{
   this.setState({ categories: responce.data });
}).catch(error=>{  console.log(error)   })

 }


 showCategories() {
  if (this.state.categories == undefined) {   
    return <div>wait</div>;
} else {
    return (    
    this.state.categories.map((link,key) => {
      return(

        <ListItem button  onClick={() => this.Filter(link)} style={{height:10}}>
          <ListItemIcon><InboxIcon /></ListItemIcon>
          <ListItemText primary={link}/>
        </ListItem>
      );
  })  ) ; }
 
 }



 


showData() {
   if (this.state.jokes == undefined) {   
    return <div>wait</div>;
} else {
    return (
    
    
    
    this.state.jokes.map((link,key) => {
      return(
      <Paper   elevation={5} style={{margin:40,padding:25}}>
          <Typography variant="h4" component="h3">

                <Link 
                style={{color:'#000',textDecoration: 'none' }}
                to={{
                pathname:  'joke'+"/"+link.id ,
                state: { value: link.value  }
                }} >{ link.value } </Link> 
          </Typography>
          <Typography component="p">
          jokes from chucknorris.io.
          </Typography>
      </Paper>
   
  
     
      );
  })  ) ; 
  
}

 


}

 

  render() {
    return (

          <Grid container spacing={12} style ={{backgroundColor:'#e3ecf9'}}>

            <LoadingScreen
    loading={this.state.loading}
    bgColor='#f1f1f1'
    spinnerColor='#9ee5f8'
    textColor='#676767'
    logoSrc='https://assets.chucknorris.host/img/avatar/chuck-norris.png'
    text='Loading'
  ></LoadingScreen>
            <Grid item xs={6}>
            <Paper  disableSticky={false}> <List  style={{position:"fixed",fontSize:12}}component="nav"  disableSticky={false}> {this.showCategories()}</List></Paper>
            </Grid>
            <Grid item xs={6}>
            <Paper  >  {this.showData()}</Paper>
            </Grid>
          </Grid>
    );
  }
}

export default Feed;